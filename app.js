new Vue({
    el: '#app',
    data: {
        gameRunning: false,
        personHealth: 100,
        monsterHealth: 100,
        results: [],
    },

    methods: {
        startGame() {
            this.gameRunning = true;
            this.personHealth = 100;
            this.monsterHealth = 100;
        },
        endGame() {
            this.gameRunning = false;
            this.personHealth = 100;
            this.monsterHealth = 100;
            this.results = [];



        },
        attack() {

            this.attackByMonster(0, 10);


            // monster health attack percentage
            var monsterDamage = Math.max(Math.floor(Math.random() * 8) + 1, 0);
            this.results.unshift('Attack by You - ' + monsterDamage);
            if (this.monsterHealth > 10) {
                this.monsterHealth -= monsterDamage;
            } else if (this.monsterHealth <= 10 && this.monsterHealth >= 6) {
                this.monsterHealth -= 5;
            } else {
                if (this.monsterHealth < 0 || this.monsterHealth <= 5) {
                    this.monsterHealth = 0;
                }
            }
            this.checkWin();

        },
        specialAttack() {

            this.attackByMonster(0, 10);
            // monster health attack percentage
            var monsterDamage = Math.max(Math.floor(Math.random() * 12) + 1, 0);
            this.results.unshift('Attack by You - ' + monsterDamage);

            if (this.monsterHealth > 10) {
                this.monsterHealth -= monsterDamage;
            } else if (this.monsterHealth <= 10 && this.monsterHealth >= 6) {
                this.monsterHealth -= 5;
            } else {
                if (this.monsterHealth < 0 || this.monsterHealth <= 5) {
                    this.monsterHealth = 0;
                }
            }
            this.checkWin();


        },
        heal() {

            var personHealthIncrease = Math.max(Math.floor(Math.random() * 10) + 1, 0);
            this.results.unshift('heal  You - ' + personHealthIncrease);

            //personHealth attack percentage
            if (this.personHealth < 100) {
                this.personHealth += personHealthIncrease;
            } else if (this.personHealth >= 90 && this.personHealth <= 96) {
                this.personHealth += 5;
            } else {
                if (this.personHealth > 99 || this.personHealth >= 95) {
                    this.personHealth = 100;
                }
            }
            this.attackByMonster(0, 10);

            var monsterDamage = Math.max(Math.floor(Math.random() * 10) + 1, 0);
            this.results.unshift('Attack by You - ' + monsterDamage);

            if (this.monsterHealth > 10) {
                this.monsterHealth -= monsterDamage;
            } else if (this.monsterHealth <= 10 && this.monsterHealth >= 6) {
                this.monsterHealth -= 5;
            } else {
                if (this.monsterHealth < 0 || this.monsterHealth <= 5) {
                    this.monsterHealth = 0;
                }
            }


            this.checkWin();

        },
        attackByMonster(min, max) {

            var Damage = Math.max(Math.floor(Math.random() * max) + 1, min);
            this.results.unshift('Attack by Monster - ' + Damage);
            if (this.personHealth > 10) {
                this.personHealth -= Damage;
            } else if (this.personHealth <= 10 && this.personHealth >= 6) {
                this.personHealth -= 5;
            } else {
                if (this.personHealth < 0 || this.personHealth <= 5) {
                    this.personHealth = 0;
                }
            }

        },
        checkWin() {
            if (this.monsterHealth == 0 && this.personHealth > 0) {
                alert('YOU ARE WON');
                this.endGame();
            }
            if (this.personHealth == 0 && this.monsterHealth > 0) {
                alert('YOU LOSS');
                this.endGame();
            }
        }

    },

})